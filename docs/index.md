# Large Radius Tracking

Welcome to large radius tracking archive.
This project is aim to provide a space to store toolset 
and model archives of GNN large radius tracking 
for future reference and result reproduction.

## Exa.TrkX Pipeline

This project is based on [Exa.TrkX pipeline](https://github.com/HSF-reco-and-software-triggers/Tracking-ML-Exa.TrkX).
Recent study have shown Exa.TrkX pipeline achives good performance 
in finding tracks originted from the interaction point.
We have tried to apply the pipeline to large radius tracks,
and you can try to reproduce those result in this project.

## Components

The project is separate into 3 parts, Toolset, Models and Analysis.
Toolset contains some useful script to handle large radius dataset.
Models contains code and configurations of pipeline.
Analysis contains post-training analysis tools.

## Get Strarted

To learn basic ideas of GNN particle tracking and Exa.TrkX pipeline,
see [Concepts](https://gnnparticletracking.gitlab.io/largeradiustracking/docs/concepts).
To get started, see [Setup](https://gnnparticletracking.gitlab.io/largeradiustracking/docs/setup/) for more information.

## License

Model source code are belong to Exa.TrkX group.
It is contained in this repo for archiving and future reference.

