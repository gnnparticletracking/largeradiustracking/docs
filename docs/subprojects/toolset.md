# Toolset

Toolset repository is located [here](https://gitlab.com/gnnparticletracking/largeradiustracking/toolset).
This project provide some useful scripts to handle LRT data, 
including data preprocessing, hyperparameter tuning and more.
All scripts should run in same environment of pipeline.

## TrainTrack

TrackTrack is a python module to run pipeline and should be installed along with Exa.TrkX pipeline.
However, some tweaks is required to run certain task such as multi-GPU training.

## Preprocessing

- to_pyg2.py

    LRT dataset is originally created using PyTorch Geometric 1.7.x,
    which is incompatible with PyG 2.
    It is recommended to convert dataset to PyG 2 format.

- normalize.py

    It turn out that GNN will perform better if we normalize spacepoint coordinates first.
    This script will also append additional information such as pt, eta, etc.
    This requires event particle information contained in flile named with particle-xxxxxxx.csv.
    This function will separate to another script in near feature.

## Embedding

- rk_scan.py

    Embedding stage using radius *r* and number of near neighbors *k* in FRNN to control how many edges will be constructed.
    This script help to test trained model with different *r* and *k* value combinations.
    
## Filter

- cut_scan.py
    
    Filter stage using cut value to determine which edge is false edge and should be remove.
    This script help to test trained model with different cut values.

