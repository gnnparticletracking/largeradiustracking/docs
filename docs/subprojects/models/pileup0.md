# Pileup 0

This is an early version of LRT GNN model.
Input data is Heavy Neutral Lepton process event with HNL mass = 15 GeV, lifetime = 100 mm without pileup.
Note that input features are normalized cylindrical coordinates of hits with scale (3000, pi, 400).
It use 10K events for training and validation.
Another 5K events is used for testing.
Test dataset contains particle information to classify prompt and displaced tracks.
Both dataset can be find [Zenodo](https://zenodo.org/record/6812533).

## Dataset Preprocessing

To preprocess data before running pipeline, you need:

1. Convert dataset into PyTorch Geometric 2.x format.
    
    LRT dataset provided in Zenodo is saved as npz format. 
    Conversion using `preprocessing/npz2pyg.py` is required.
        
    If you have older version of dataset which is created by PyG 1.7.x,
    it is still recommended to convert dataset into PyG 2.x format using `preprocessing/pyg2conv.py`.

2. Renormalize spacepoint coordinates.

    It was find that model perform better if we normalize spacepoint coordinates
    before feed into pipeline.

3. Add transverse momentum (pt) data to dataset.

    If we don't consider pt cut, then pt information is unecessary.
    However, model will try to get pt information 
    even when we set pt cut equal to zero.
    A strightforward solution is add pt information to dataset and
    this can be done if we have particle information.

## Configurations

### Robustness Test

Additional configurations are included for robustness test, 
including ttbar, HNL10GeV100mm and HNL15GeV200mm events.

## Publications

Trained model with this configuration was used to generate result for poster and 
[preceeding](https://arxiv.org/abs/2203.08800) 
of 
[ACAT 2021](https://indico.cern.ch/event/855454).

