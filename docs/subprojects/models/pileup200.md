# Pileup 200

The pileup 200 configurations use private v5 dataset,
containing 10K Heavy Neutral Lepton process pileup 200 event with HNL mass = 15 GeV, lifetime = 100 mm.
Note that v5 dataset input cylindrical coordinates of hits is already normalized with scale (3000, pi, 400).

## Cluster Shape Information

Due to the poor performance, cluster shape information, or cell data, 
is included as input features beside three dimension coordinate of hits to improve model.
Four type of cluster shape information, 
cell width, cell height, cell count and cell val 
are used in these model.

## Dataset Preprocessing

To preprocess data before running pipeline, you need:

1. Convert dataset into PyTorch Geometric 2.x format.
    
    LRTv5 dataset is saved as npz format. 
    Conversion using `preprocessing/npz2pyg.py` is required.
        
2. Add transverse momentum (pt) data to dataset.

## Configurations

### Full Training
    
It use 3 dimension coordinates and 4 cluster shape information as input features.

### Cluster Shape Information
    
To proof cluster shape information can improve model, 
additional model are trained using small portion of dataset (1000 events) with 
no cluster shape information.

Such investigation is also done for non-pileup case using v2 dataset.
Additionally, use only 3 cluster shape information 
(without cell val, since it is poor simulated)
also performed.

