# Debugging

This section list some common errors and how to solve it.

- I cannot see any output from stage.
    
    Output are built by Callbacks.
    Check if you have correct callback setup in stage config.
    For example, you should have

    ```
    callbacks: [[GNNBuilder]]
    ```

    in your GNN stage config.

- `The 'data' ojbect was created by an older version of PyG.`
    
    Covert you date into PyG 2.x format. See Toolset for more information.

- `'GlobalStorage' object has no attribute 'pt'`

    Add pt column to data. See Toolset for more information.
    If problem still happening, investigate the code and see if anything is mis-configed.

- `'GlobalStorage' object has no attribute 'xxx'`
    
    Investigate the code and see if anything is mis-configed.
    Some code might access wrong column in dataset.

- `self.hparams["xxx"] KeyError:'xxx'`

    Some options are missing from you configuration file.
    You should check your config file.

- `Sum of input lengths does not equal the length of the input dataset`

    Number of files is not matched in your config.
    First, check if input directory is correct.
    Wrong directory might give 0 input file, causing wrong input number of data.
    Second, check your `train_test_split` option is correct.
    Sum of number of train, test and validation data should match total number of input data.
    Note that this should only required by embedding stage.
    For other stages, smaller number of `train_test_split` 
    will cause stage use small portion of data in input directory.

