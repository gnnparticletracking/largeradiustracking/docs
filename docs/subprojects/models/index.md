# Models 

Models repository is located [here](https://gitlab.com/gnnparticletracking/largeradiustracking/models).
This project contains model training and testing configurations and code archives.

To setup any of them, you can follow the instructions: 

1. Accquire Dataset

    To reproduce result of recent LRT study, 
    you need to accquire desired dataset for training and testing first.
    Each model require different dataset and preprocessing. 
    Most of dataset are public and can be find at [Zenodo](https://zenodo.org/record/6812533),
    otherwise you need to contact Exa.TrkX group to accquire private dataset.
    On the other hand, all preprocessing scripts are placed in
    [Toolset](https://gitlab.com/gnnparticletracking/largeradiustracking/toolset).
    See section of each model to learn more detail.

2. Copy Model and Configurations

    To integrate model into pipeline, you need to copy model directory to `Tracking-ML-Exa.TrkX/Pipelines`.
    Typically, each model directory contains `configs` and `LightningModules`:

    - `configs`

        `configs` directory contains project config, batch config and pipeline config:
        
        - Project config includes model code and output location.
            It also contains logger backend options.
        - Batch config includes option to run grid search for hyperparameters on NERSC.
        - Pipeline config includes which stage will be run and config location for each stage.

    - `LightningModules`

        `LightningModules` contains code of models and stage configurations.

    You may want to revisit each configuration file, especially input and output directory.

3. Run Pipeline

    Run pipeline with `traintrack path/to/pipeline/train/config`.

4. Analysis

    Analyse result using Analysis repo.
    See [Analysis](https://gitlab.com/gnnparticletracking/largeradiustracking/analysis) for more information.

# License

Model source code are belong to Exa.TrkX group.
It is contained in this repo for archiving and future reference.

