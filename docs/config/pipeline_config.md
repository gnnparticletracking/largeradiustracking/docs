# Pipeline Config

## Stages

There are 3 main stages to configure in this file.
 
1. Embedding
2. Filter
3. GNN

##Example

Below configs the filter stage for example.

```
- {set: Filter, name: VanillaFilter, config: train.yaml,  resume_id: , batch_config: configs/batch_gpu_default.yaml, batch_setup: True, override: {callbacks: [FilterBuilder]}}
```

##Config Options

- "config:" asserts the yaml config file to be used in the Lightning Modules. 
- "resume_id:" can put lightning checkpoints here to start at a previous stopping point
- "batch_config:" is used to define watch batch yaml file should be used
- "batch_setup:" 
- "override:"
