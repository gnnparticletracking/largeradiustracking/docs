# Advanced Topics and Technical Details

## Grid Search

If you are on NERSC or similar computational platform 
using Slurm as job and resource management system,
a job can be submit to Slurm to run.
In this case, you can do grid search for hyperparameters by submitting multiple jobs automatically.
To do so, some steps need to be done.

1. Edit your batch configuration
    
    This config are same as your job script header 
    submitted to Slurm.
    Edit this to fit your need.
    Detail options can refer to NERSC or Slurm documentation.

2. Edit your stage configuration

    For example, if you want to scan 
    hidden_channels hyperparameter with 32, 64 and 96,
    you can edit hidden_channels option in stage config with an array:

    ```
    hidden_channels: [32, 64, 96]
    ```

    Mutiple hyperparameter can be configured.
    The resulting jobs will be all combinations of configured hyperparameter for grid search.
    

3. Run training with 

    ```
    traintrack path/to/pipeline/config --slurm
    ```

    This will submit multiple jobs.

Note that output models are overwrite each other in grid search for recent version of traintrack.
You might find that some output are missing.
I don't know how to solve it currently.
Use with causion.

## Multi-GPU Training

Because trainer of pipeline is created by traintrack package,
you need to tweak traintrack to enable multi-GPU training.
The traintrack package should located under 

```
~/.conda/envs/<Name of Your Conda Env>/lib/python<Python Version>/site-packages/traintracks
```

if you follow the install guide of the Exa.TrkX pipeline.

You need to tweak two things:

1. Add following code after the last import statement of `utils/model_utils.py`: 

        from pytorch_lightning.plugins import DDPPlugin, DDP2Plugin, DDPSpawnPlugin
        from pytorch_lightning.overrides import LightningDistributedModule
        
        class CustomDDPPlugin(DDPPlugin):
            def configure_ddp(self):
                self.pre_configure_ddp()
                self._model = self._setup_model(LightningDistributedModule(self.model))
                self._register_ddp_hooks()
                self._model._set_static_graph()

2. Find the following section in `utils/model_utils.py`
    
        # Handle resume condition
        if model_config["resume_id"] is None:
            # The simple case: We start fresh
            trainer = pl.Trainer(
                max_epochs=model_config["max_epochs"],
                gpus=gpus,
                logger=logger,
                callbacks=callback_objects(model_config)+[checkpoint_callback],
            )
        else:
            num_sanity = model_config["sanity_steps"] if "sanity_steps" in model_config else 2
            # Here we assume
            trainer = pl.Trainer(
                resume_from_checkpoint=model_config["checkpoint_path"],
                max_epochs=model_config["max_epochs"],
                gpus=gpus,
                num_sanity_val_steps=num_sanity,
                logger=logger,
                callbacks=callback_objects(model_config)+[checkpoint_callback],
            )

    and add strategy parameter as `CustomDDPPlugin` for trainer initialization:

        # Handle resume condition
        if model_config["resume_id"] is None:
            # The simple case: We start fresh
            trainer = pl.Trainer(
                max_epochs=model_config["max_epochs"],
                gpus=gpus,
                logger=logger,
                strategy=CustomDDPPlugin(find_unused_parameters=False),
                callbacks=callback_objects(model_config)+[checkpoint_callback],
            )
        else:
            num_sanity = model_config["sanity_steps"] if "sanity_steps" in model_config else 2
            # Here we assume
            trainer = pl.Trainer(
                resume_from_checkpoint=model_config["checkpoint_path"],
                max_epochs=model_config["max_epochs"],
                gpus=gpus,
                num_sanity_val_steps=num_sanity,
                logger=logger,
                strategy=CustomDDPPlugin(find_unused_parameters=False),
                callbacks=callback_objects(model_config)+[checkpoint_callback],
            )

3. Find the following line in `utils/model_utils.py`
    
    ```
    gpus = 1 if torch.cuda.is_available()
    ```

    Replace it with

    ```
    gpus = torch.cuda.device_count()
    ```

Now you can train with multiple GPU.
Note that you can change how many GPU want to use by change `gpus` variable.

Thanks for Daniel Murnane to provide DDPPlugin code snippet.

## Tensorflow and Graph-net

In some situation, you may want to use Tensorflow instead of PyTorch.
For example, you want to use Tensorflow Large Model Support.

