# Project Setup

To use any component in this project, you need to follow the 
[installation guide](https://hsf-reco-and-software-triggers.github.io/Tracking-ML-Exa.TrkX/)
from Exa.TrkX pipeline to setup a conda environment.
It should work out-of-box if you don't have any special purpose.
However, some general notes is listed in following sections.

## GPU

Using GPU device is recommanded 
since relative large datasets are used in this project.
If you planning to install pipeline with GPU support, you should check you CUDA library with

```
nvcc -v
```

After installation, you can check PyTorch version 
by running following commands in your python interpreter: 

```
import torch

# Check torch version.
# It should output some like "<PyTorch Version>+cu<Cuda Version>"
print(torch.__version__)

# Check if torch recognize your GPU device.
print(torch.cuda.is_available())

# How many GPU is avaliable.
# If you have multiple GPU, see Muti-GPU Traing in Advanced Topics for detail.
print(torch.cuda.device_count())
```

Make sure everything is matched.

## NERSC Environment

This project is done by using NERSC computational resources.
If you are in NERSC or similar computational platform,
Python and CUDA should be pre-installed. 
Refer to platform documataion to learn how to activate them.
Take NERSC Perlmutter for example, you need to activate it by using

```
module load cudatoolkit cudnn python
```

Then you can access pre-install conda environment manager.
Also, environment should also be activated if you submit your job
to job management system such as Slurm.

## Python Virtual Environment

If you prefer python virtual environment tools, such as virtualenv, venv, pipenv, etc.,
you may want to use it instead of conda.
In this case, you should aware that some packages of PyTorch Geometric is not in PyPI.
It is recommended to read though 
[PyG installation guide](https://pytorch-geometric.readthedocs.io/en/latest/notes/installation.html)
before you setup your virtual environment. 
Make sure you install PyG with correct CUDA version.

