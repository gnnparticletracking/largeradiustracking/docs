# Attention Graph Stage

## The Generated Graph
After running through Embedding and Filter stages a constructed graph is given to GNN.
This consists of a list (Vertices, Edges) which can be represented as a matrix.
Vertex information consists of the spatial coordinates (or cell data if desired).
Edge should connect all nodes of same track (hopefully).


## The Attention Graph Network

Attention on specific nodes is achieved by adding weight parameter to each edge, this is applied to node update which gives parameter to apply importance to each neighbor node.
The message propogation is done by MLP with node features as input.
This process is permutation invariant! It does not matter the direction you apply edge update and node update.

