# Embedding Stage

## Major Parts of Embedding Stage to Understand

1. MLP
2. Construction of Prediction Graph
3. Hinge Embedding Loss

### Multi Layer Perceptron (MLP)

This a machine learning layer that consists of a input layer of 3 neurons (3 spacepoints) and a arbitrary output layer of a higher dimensional space. 

![alt text](Embedding_pic.png)

### Construct Prediction Graph

Use FRNN algorithm to calculate nearest neighbors up to maximum within certain radius. All the found neighbors are connected with edges. Further add extra edges for stability with random edges and hard negative mining edges.

![alt text](Similarity.png)


### Hinge Embedding Loss

First, using the genereated prediction graph find the intersection of edges of truth pairs.
If this is a truth pair use positive hinge loss.
If this is a false edge use negative hinge loss.
The goal of this process is to move points of the same track closer together and into a line.
Then to move points of opposite tracks further away.

\begin{equation}
l_n = \begin{cases}
	x_n 				& \text{if $y_n = 1$}  \\
    max\{0, 1-x_n\} 	& \text{if $y_n = -1$} \\
\end{cases}
\end{equation}

