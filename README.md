# Large Radius Tracking Archive

See [documentation](https://gnnparticletracking.gitlab.io/largeradiustracking/docs/)
for more information.

# License

The documentation is published under MIT license. 
The MathJax library is under Apache license.

Some code and plots in documents are from Exa.TrkX pipeline and published papers. 
See reference page for details.

